

git init (optional)
git status (optional)
git log (optional)
git remote -v (optional)

locally:
git add .
git commit -m "message"


remotely:
git remote add <alias> <ssh link>
git push origin master